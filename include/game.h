#ifndef __GAME_H__
#define __GAME_H__

#include "common.h"

/* 主循环 */
void main_loop(void);

void redraw_screen(void);

/* 随机数 */
int rand(void);
void srand(int seed);

#endif
