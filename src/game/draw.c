#include "game.h"
#include "string.h"
#include "device/video.h"

/* 绘制屏幕上的内容。
 * 注意程序在绘图之前调用了prepare_buffer，结束前调用了display_buffer。
 * prepare_buffer会准备一个空白的绘图缓冲区，display_buffer则会将缓冲区绘制到屏幕上，
 * draw_pixel或draw_string绘制的内容将保存在缓冲区内(暂时不会显示在屏幕上)，调用
 * display_buffer后才会显示。
*/
void  
redraw_screen() 
{	
	/*Complete here.Just only some lines code*/
	
	prepare_buffer();
	int px,py=0;
	for (px=0;px<SCR_HEIGHT;px++)
		for(py=0;py<SCR_WIDTH;py++)
			draw_pixel(px,py,5);
	draw_string("Welcome to enter OS",90,80,1);
	display_buffer();

}

