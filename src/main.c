#include "game.h"
#include "x86/x86.h"
#include "assert.h"

void
game_init(void) 
{
	main_loop();
	assert(0); /* main_loop是死循环，永远无法返回这里 */
}
